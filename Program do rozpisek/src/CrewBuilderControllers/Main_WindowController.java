package CrewBuilderControllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * Created by Mateusz on 2017-02-18.
 */
public class Main_WindowController {

    @FXML
    private Pane mainPane;

    @FXML
    public void initialize() {
        loadMainScreen();
    }

    public void loadMainScreen() {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/CrewBuilderFXML/Main_MenuFXML.fxml"));
        AnchorPane anchorPane = null;
        try {
            anchorPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Main_MenuController main_menuController = loader.getController();
        main_menuController.setMain_windowController(this);
        setScreen(anchorPane);
    }

    public void setScreen(AnchorPane anchorPane) {
        mainPane.getChildren().clear();
        mainPane.getChildren().add(anchorPane);
    }
}
