package CrewBuilderControllers;

import Program_glowne.Main_Window;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

/**
 * Created by Mateusz on 2017-02-18.
 */
public class Main_MenuController {

    private Main_WindowController main_windowController;

    @FXML
    public void openCrewBuilderWindow(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/CrewBuilderFXML/Crew_Selector_WindowFXML.fxml"));
        AnchorPane anchorPane = null;
        try {
            anchorPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Crew_SelectorController crew_selectorController = loader.getController();
        crew_selectorController.setMain_windowController(main_windowController);
        main_windowController.setScreen(anchorPane);
    }

    public void openEncounterSetupWindow(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/CrewBuilderFXML/Encounter_SetupFXML.fxml"));
        AnchorPane anchorPane = null;
        try {
            anchorPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Encounter_SetupController encounter_setupController = loader.getController();
        encounter_setupController.setMain_windowController(main_windowController);
        main_windowController.setScreen(anchorPane);

    }

    public void openCollectionWindow(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/CrewBuilderFXML/CollectionControllerFXML.fxml"));
        AnchorPane anchorPane = null;
        try {
            anchorPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        CollectionController collectionController = loader.getController();
        collectionController.setMain_windowController(main_windowController);
        main_windowController.setScreen(anchorPane);
    }

    public void exitProgram(ActionEvent actionEvent) {
        System.exit(0);
    }


    public void setMain_windowController(Main_WindowController main_windowController) {
        this.main_windowController = main_windowController;
    }
}
