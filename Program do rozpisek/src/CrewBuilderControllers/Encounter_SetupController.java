package CrewBuilderControllers;

import javafx.event.ActionEvent;

/**
 * Created by Mateusz on 2017-02-21.
 */
public class Encounter_SetupController {
    private Main_WindowController main_windowController;

    public void goBackToMainMenu(ActionEvent actionEvent) {
        main_windowController.loadMainScreen();
    }

    public void setMain_windowController(Main_WindowController main_windowController) {
        this.main_windowController = main_windowController;
    }
}
