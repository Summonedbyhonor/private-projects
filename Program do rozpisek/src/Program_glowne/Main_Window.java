package Program_glowne;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Created by Mateusz on 2017-02-18.
 */
public class Main_Window extends Application {
    private Parent parent;
    private Scene scene;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/CrewBuilderFXML/Main_WindowFXML.fxml"));
        Pane pane = loader.load();


        scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Crew Builder");
        primaryStage.show();
    }


}
